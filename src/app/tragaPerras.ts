import { Component, OnInit } from '@angular/core';
export class TragaPerras implements OnInit{
	private elJson = {
			'Nombre': ["cereza", "limon", "pina", "platano", "sandia"], 
			'NombreFichero': ["cereza.PNG", "limon.PNG", "pina.PNG", "platano.PNG", "sandia.PNG"],
			 'url': "./assets/img/"};
	private monedas:number=0;
	private nMonedas:number=0;
	private n:number=0;
	private divFotos:any;
	private resultadoTirada:Array<string>=[];
	//premio 0: 1 cereza
	//premio 1: 2 cerezas
	//premio 2: 3 cerezas
	//premio 3: dos frutas iguales que no sean cerezas
	//premio 4: 3 frutas iguales que no sean cerezas
	//premio 5: cereza y dos frutas iguales
	private premio:Array<number>=[1,4,10,2,5,3,-1];
	
	//private list: Array<number> = [1, 2, 3];
	private nCerezas:number=0;
	private mPremio=0;
	constructor(selector:string){
		console.log("selector vale "+selector);
		window.addEventListener("load", (e)=>{
			this.divFotos=selector;
			this.divFotos=document.querySelectorAll(selector);
			//console.log(selector);
			this.actualizaTragaperras();

		});

	}
	sumaPremio(n){
		//console.log("nomedas vale "+nMonedas)
		this.nMonedas+=n;
	}
	devuelvemPremio(){
		return this.mPremio;
	}
	devuelveNombres(){
		return this.elJson.Nombre;
	}
	devuelveNombresFichero(){
		return this.elJson.NombreFichero;
	}
	devuelveUrl(){
		return this.elJson.url;
	}
	sumaMoneda(){
		this.nMonedas+=1;
	}
	restaMoneda(){
		this.nMonedas-=1;
	}
	restaNMonedas(n){
		this.nMonedas-=n;
	}
	sumaNMonedas(n){
		this.nMonedas+=n;
	}
	getMonedas(){
		return this.nMonedas;
	}
	//se le pasa la lista con el selector donde iran las imagenes
	actualizaTragaperras(){
		let n:number=0;
		for(var i = 0; i < this.divFotos.length; i++){
			n=Math.floor((Math.random() * 5) + 1);
			this.divFotos[i].innerHTML='<img src="'+this.elJson.url+this.elJson.NombreFichero[n-1]+'" alt="">';
			this.resultadoTirada[i]=this.elJson.Nombre[n-1];
			console.log("n vale "+n);

		}
		console.log("resultadoTirada vale");
		console.log(this.resultadoTirada);
		this.compruebaPremio();
	}
	compruebaPremio(){
		let indicePremio:number=6;
		for(let i=0; i<this.resultadoTirada.length; i++){
			if(this.resultadoTirada[i]=="cereza"){
			this.nCerezas+=1;

			}
		}
		if(this.nCerezas==1){
			indicePremio=0;
			if(this.resultadoTirada[0]==this.resultadoTirada[1] || this.resultadoTirada[0]==this.resultadoTirada[2] || this.resultadoTirada[1]==this.resultadoTirada[2] ){
					indicePremio=5;
				}
		}
		if(this.nCerezas==2){
			indicePremio=1;
		}
		if(this.nCerezas==3){
			indicePremio=2;
		}
		if(this.nCerezas==0){
			if(this.resultadoTirada[0]==this.resultadoTirada[1] || this.resultadoTirada[0]==this.resultadoTirada[2] || this.resultadoTirada[1]==this.resultadoTirada[2] ){
					indicePremio=3;
				}
			if(this.resultadoTirada[0]==this.resultadoTirada[1] && this.resultadoTirada[1]==this.resultadoTirada[2] ){
					indicePremio=4;
				}
		}
		//console.log("Total cerezas "+this.nCerezas);
		console.log("monedas del premio "+this.premio[indicePremio]);
		this.mPremio=this.premio[indicePremio];
		console.log("y devuelvemPremio "+this.devuelvemPremio());
		this.nCerezas=0;
		indicePremio=6;
		
	}
	ngOnInit() {
		console.log("llega al ngOninit sin llamarlo");
  }
}