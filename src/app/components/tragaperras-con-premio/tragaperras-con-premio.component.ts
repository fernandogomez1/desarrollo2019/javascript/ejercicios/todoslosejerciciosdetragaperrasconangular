import { Component, OnInit } from '@angular/core';
import { TragaPerras } from '../../tragaPerras';

@Component({
  selector: 'app-tragaperras-con-premio',
  templateUrl: './tragaperras-con-premio.component.html',
  styleUrls: ['./tragaperras-con-premio.component.css']
})
export class TragaperrasConPremioComponent implements OnInit {
public tp:TragaPerras=new TragaPerras(".foto");
  constructor() { 

		let btnMonedas, divCara, divPremio, divMonedas;
		let nMonedas=0;
		let btnJugar;

		window.addEventListener("load", (e)=>{
			divCara=document.querySelector("#cara");
			divPremio=document.querySelector("#premio");
			divMonedas=document.querySelector("#nMonedas");
			divMonedas.innerHTML=this.tp.getMonedas();

			btnJugar=document.querySelector("#jugar");

			btnJugar.addEventListener("click", (e)=>{
				if(this.tp.getMonedas()<1){
						window.alert("introduce monedas");
					}else{
						this.tp.actualizaTragaperras();
						this.tp.sumaPremio(this.tp.devuelvemPremio());
						divMonedas.innerHTML=this.tp.getMonedas();
						if((this.tp.devuelvemPremio())<0){
							divCara.innerHTML='<img src="./assets/img/caraTriste.PNG" alt="">';

						}else{
							divCara.innerHTML='<img src="./assets/img/caraAlegre.PNG" alt="">';
						}
						divPremio.innerHTML=this.tp.devuelvemPremio();
						
					}
			});

			btnMonedas=document.querySelector("#meterMoneda");
			btnMonedas.addEventListener("click", (e)=>{
					this.tp.sumaMoneda();
					divMonedas.innerHTML=this.tp.getMonedas();
			});
		});
  }

  ngOnInit() {
  }

}

