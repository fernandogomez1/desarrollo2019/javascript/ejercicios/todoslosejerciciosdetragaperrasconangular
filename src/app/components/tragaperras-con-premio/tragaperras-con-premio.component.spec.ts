import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TragaperrasConPremioComponent } from './tragaperras-con-premio.component';

describe('TragaperrasConPremioComponent', () => {
  let component: TragaperrasConPremioComponent;
  let fixture: ComponentFixture<TragaperrasConPremioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TragaperrasConPremioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TragaperrasConPremioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
