import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TragaperrasConGastoComponent } from './tragaperras-con-gasto.component';

describe('TragaperrasConGastoComponent', () => {
  let component: TragaperrasConGastoComponent;
  let fixture: ComponentFixture<TragaperrasConGastoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TragaperrasConGastoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TragaperrasConGastoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
