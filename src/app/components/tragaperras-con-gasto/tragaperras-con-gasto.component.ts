import { Component, OnInit } from '@angular/core';
import { TragaPerras } from '../../tragaPerras';

@Component({
  selector: 'app-tragaperras-con-gasto',
  templateUrl: './tragaperras-con-gasto.component.html',
  styleUrls: ['../tragaperras-con-monedas/tragaperras-con-monedas.component.css']
})
export class TragaperrasConGastoComponent implements OnInit {
public tp:TragaPerras=new TragaPerras(".foto");
  constructor() { 

		let btnMonedas;
		let nMonedas=0;
		let divMonedas;
		let btnJugar;
		window.addEventListener("load", (e)=>{
			
			divMonedas=document.querySelector("#nMonedas");
			divMonedas.innerHTML=this.tp.getMonedas();

			btnJugar=document.querySelector("#jugar");

			btnJugar.addEventListener("click", (e)=>{
				if(this.tp.getMonedas()<1){
						window.alert("introduce monedas");
					}else{
						this.tp.actualizaTragaperras();
						this.tp.restaMoneda();
						divMonedas.innerHTML=this.tp.getMonedas();
					}
			});

			btnMonedas=document.querySelector("#meterMoneda");
			btnMonedas.addEventListener("click", (e)=>{
					this.tp.sumaMoneda();
					divMonedas.innerHTML=this.tp.getMonedas();
			});
		});
  }

  ngOnInit() {
  }

}
