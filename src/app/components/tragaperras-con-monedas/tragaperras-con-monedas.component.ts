import { Component, OnInit } from '@angular/core';
import { TragaPerras } from '../../tragaPerras';

@Component({
  selector: 'app-tragaperras-con-monedas',
  templateUrl: './tragaperras-con-monedas.component.html',
  styleUrls: ['./tragaperras-con-monedas.component.css']
})
export class TragaperrasConMonedasComponent implements OnInit {
public tp:TragaPerras=new TragaPerras(".foto");
  constructor() {

		let btnMonedas;
		let nMonedas=0;
		let divMonedas;
		window.addEventListener("load", (e)=>{
			
			divMonedas=document.querySelector("#nMonedas");
			divMonedas.innerHTML=this.tp.getMonedas();
			//actualizaTragaperras();
			
			
			//console.log("el valor nombre es "+json.Nombre[0]);

			btnMonedas=document.querySelector("#meterMoneda");
			btnMonedas.addEventListener("click", (e)=>{
					this.tp.actualizaTragaperras();
					this.tp.sumaMoneda();
					divMonedas.innerHTML=this.tp.getMonedas();
			});
		});
		
   }

  ngOnInit() {
  	//se inicia la tragaPerras indicando el selector
   	console.log("llega al oninit de compnente");
   	//this.tp.actualizaTragaperras(".foto");
  }

}
