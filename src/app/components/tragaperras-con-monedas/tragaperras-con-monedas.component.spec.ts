import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TragaperrasConMonedasComponent } from './tragaperras-con-monedas.component';

describe('TragaperrasConMonedasComponent', () => {
  let component: TragaperrasConMonedasComponent;
  let fixture: ComponentFixture<TragaperrasConMonedasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TragaperrasConMonedasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TragaperrasConMonedasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
