import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TragaperrasConMonedasComponent } from './components/tragaperras-con-monedas/tragaperras-con-monedas.component';
import { TragaperrasConGastoComponent } from './components/tragaperras-con-gasto/tragaperras-con-gasto.component';
import { TragaperrasConPremioComponent } from './components/tragaperras-con-premio/tragaperras-con-premio.component';
import { TragaperrasConPremioYApuestaVariableComponent } from './tragaperras-con-premio-y-apuesta-variable/tragaperras-con-premio-y-apuesta-variable.component';

@NgModule({
  declarations: [
    AppComponent,
    TragaperrasConMonedasComponent,
    TragaperrasConGastoComponent,
    TragaperrasConPremioComponent,
    TragaperrasConPremioYApuestaVariableComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
