import { Component, OnInit } from '@angular/core';
import { TragaPerras } from '../tragaPerras';

@Component({
  selector: 'app-tragaperras-con-premio-y-apuesta-variable',
  templateUrl: './tragaperras-con-premio-y-apuesta-variable.component.html',
  styleUrls: ['./tragaperras-con-premio-y-apuesta-variable.component.css']
})
export class TragaperrasConPremioYApuestaVariableComponent implements OnInit {
public tp:TragaPerras=new TragaPerras(".foto");
  constructor() { 

		let btnMonedas, divCara, divPremio, divMonedas, divMonedasApuesta,btnApostar,monedasApuestas;
		let btnJugar;
		let mApuesta=0;

		window.addEventListener("load", (e)=>{
			divMonedasApuesta=document.querySelector("#monedasApuesta");
			divCara=document.querySelector("#cara");
			divPremio=document.querySelector("#premio");
			divMonedas=document.querySelector("#nMonedas");
			divMonedas.innerHTML=this.tp.getMonedas();

			btnJugar=document.querySelector("#jugar");

			btnJugar.addEventListener("click", (e)=>{
				if(this.tp.getMonedas()<1 || mApuesta>this.tp.getMonedas()){
						window.alert("introduce monedas");
					}else{
						this.tp.actualizaTragaperras();
						this.tp.sumaPremio(this.tp.devuelvemPremio());
						divMonedas.innerHTML=this.tp.getMonedas();
						if((this.tp.devuelvemPremio())<0){
							divCara.innerHTML='<img src="./assets/img/caraTriste.PNG" alt="">';
							this.tp.restaNMonedas(mApuesta);
							mApuesta=0;
						}else{
							divCara.innerHTML='<img src="./assets/img/caraAlegre.PNG" alt="">';
							this.tp.sumaNMonedas(mApuesta);
							mApuesta=0;

						}
						divPremio.innerHTML=this.tp.devuelvemPremio();
						
						monedasApuestas.innerHTML=mApuesta;
						divMonedas.innerHTML=this.tp.getMonedas();
					}
			});

			btnMonedas=document.querySelector("#meterMoneda");
			btnMonedas.addEventListener("click", (e)=>{
					this.tp.sumaMoneda();
					divMonedas.innerHTML=this.tp.getMonedas();
			});
			btnApostar=document.querySelector("#btApostar");
			monedasApuestas=document.querySelector("#monedasApuesta");
			btnApostar.addEventListener("click", (e)=>{
				mApuesta+=1;
				monedasApuestas.innerHTML=mApuesta;
			});
		});
  }

  ngOnInit() {
  }


}
