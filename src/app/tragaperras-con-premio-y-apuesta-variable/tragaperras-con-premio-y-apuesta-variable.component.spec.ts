import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TragaperrasConPremioYApuestaVariableComponent } from './tragaperras-con-premio-y-apuesta-variable.component';

describe('TragaperrasConPremioYApuestaVariableComponent', () => {
  let component: TragaperrasConPremioYApuestaVariableComponent;
  let fixture: ComponentFixture<TragaperrasConPremioYApuestaVariableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TragaperrasConPremioYApuestaVariableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TragaperrasConPremioYApuestaVariableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
